# README #

Homepage responsive template.

## Author ##

* Alexander Chaika <manti.by@gmail.com>

## About ##

* Based on Bootstrap v4.1.3 
* Template version v1.0
* [Repository link](https://bitbucket.org/manti_by/blank/)

## CSS Guidelines ##

* Don't use id's in CSS, only in specific cases
* Page specific rules have to be prepared with namespace class in body tag
* Responsive rules work as "Mobile-to-Desktop" waterfall

## Setup hook listener ##

$ uwsgi --socket :9090 --plugins python3 --protocol uwsgi --wsgi main:application
