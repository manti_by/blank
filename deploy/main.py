import logging
import subprocess

logger = logging.getLogger()


def application(env, start_response):
    logger.info('Starting app')
    try:
        start_response('200 OK', [('Content-Type','text/html')])
        return subprocess.check_output(["./build.sh"])
    except subprocess.CalledProcessError as e:
        return [e.output]
