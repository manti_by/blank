#!/bin/bash
git pull

app_path=$(dirname $(pwd))/app
head_num=$(git rev-list --count HEAD)
tag_num=$(git rev-list --count $(git rev-list --tags --max-count=1))
revision=$(expr $head_num - $tag_num)
version=$(git describe --abbrev=0 --tags).$revision

mkdir -p $app_path/build
cd $app_path/
zip -r $app_path/build/$version.zip ./ -x build\*
